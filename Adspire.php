<?php
/**
 * Created by PhpStorm.
 * User: sergy
 * Date: 09.04.19
 * Time: 14:40
 */

namespace Bitrix\Stdin;

use Bitrix\Main\Loader;
use Spatie\ArrayToXml\ArrayToXml;

class Adspire
{
    public $orders = [];
    public $datefrom;
    public $fromdate;
    public $todate;
    public $dateto;

    public function __construct(){

        $this->datefrom = mktime(0,0,0,date('m'),1,date('Y'));
        $this->dateto = mktime(23,59,59,date('m'),date('d'),date('Y'));
    }
  
    public function getFilter(){

        $fromDate = date("d.m.Y H:i:s",
            $this->datefrom);
        $this->datefrom = $fromDate;
        $toDate = date("d.m.Y H:i:s",
            $this->dateto);
        $this->todate = $toDate;
        $arFilter =  [
            'filter' => [
                ">=DATE_STATUS" =>$fromDate,
                //"<=DATE_STATUS" =>$toDate,
            ],
            'order' => ["DATE_INSERT" => "ASC"]
        ];

        $arFilter['runtime']=[
            new \Bitrix\Main\Entity\ReferenceField(
                'PROPERTY',
                '\Bitrix\sale\Internals\OrderPropsTable',
                array("=ref.PERSON_TYPE_ID" => "this.PERSON_TYPE_ID",),
                array("join_type"=>"inner")
            ),
            new \Bitrix\Main\Entity\ReferenceField(
                'PROPS',
                '\Bitrix\sale\Internals\OrderPropsValueTable',
                array("=this.PROPERTY.ID" => "ref.ORDER_PROPS_ID", "=this.ID" => "ref.ORDER_ID",),
                array("join_type"=>"left")
            ),
        ];
        return $arFilter ;
    }
   public function create(){

       $domXML = new \DOMDocument();
        $this->getOrders();
        $xml = [

                '_attributes'=> ['date' => date('Y-m-d H:s:i')],
                'order' => $this->orders

        ];

        $result = ArrayToXml::convert($xml,'orders' , true, 'UTF-8');

        if($result){
            $domXML->loadXML($result);
            $domXML->save($_SERVER['DOCUMENT_ROOT'] .'/adspire_orders.xml');
        }
        else {
            print "result is empty";
        }

    }

    /**
     * @param Array $orders
     */
    private function setOrders($orders){
        if($orders){
            $this->orders = $orders;
        }
    }
    public function getBasket($orderID){

        $dbRes = \Bitrix\Sale\Basket::getList([
            'select' => ['NAME', 'QUANTITY','PRICE','PRODUCT_ID','ORDER_ID' ,'CURRENCY' ],
            'filter' => [
                '=ORDER_ID' => $orderID,
                '=LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
                // '=CAN_BUY' => 'Y',
            ]
        ]);
        return $items = $dbRes->fetchAll();
    }

    public function getCollection($items){
        $arResult = [];
        if(count($items)>0){

            foreach($items as $item){
                $vendor = '';
                $category = '';
                $categoryid = null;

                $res = \CCatalogSku::GetProductInfo($item["PRODUCT_ID"]);
                if($res){
                    $arSelect = Array("ID", "NAME", "IBLOCK_SECTION_ID","PROPERTY_MARKA");
                    $arFilter = Array("ID"=>$res["ID"]);
                    $res2 = \CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
                    if($product2 = $res2->Fetch()){
                        $vendor = $product2["PROPERTY_MARKA_VALUE"];
                        $id =$product2["ID"];
                        $res3 = \CIBlockSection::GetByID($product2["IBLOCK_SECTION_ID"]);
                        if($ar_res = $res3->Fetch())
                            $category = $ar_res['NAME'];
                        $categoryid = $ar_res['ID'];
                    }
                }


                $arResult[] = [
                    "cname" =>$category,
                    "cid" =>$categoryid,
                    "pid" =>$item["PRODUCT_ID"],
                    "pname" =>$item["NAME"],
                    "price" =>roundEx($item["PRICE"],0),
                    "quantity" =>roundEx($item["QUANTITY"],0),
                    "currency" => $item["CURRENCY"] ,
                    "vendor" => $vendor
                ] ;
            }
        }

        return $arResult;
    }
    public function createArrayfromOrders($orders){
        $arResult = [];
        if($orders){
            foreach($orders as $key=>$order){
                $items = $this->getCollection($this->getBasket($order["ID"]));
                if($items){
                    $arResult[$key]['items']['item'][] =  $items;
                }
                $useremail = '';
                if($order["USER_ID"]) {
                    $rsUser = \CUser::GetByID($order["USER_ID"]);
                    $arUser = $rsUser->Fetch();
                    if ($arUser["EMAIL"]) {
                        $useremail = $arUser["EMAIL"];
                    }
                }
                $db_vals = \CSaleOrderPropsValue::GetList(
                    array(),
                    array(
                        "ORDER_ID" => $order["ID"],
                        "CODE" =>  ['user_ip','atm_closer','atm_marketing','atm_remarketing'],
                    )
                );
                $propsCookie = [];
                while ($arVals = $db_vals->Fetch())
                {
                    $propsCookie[$arVals["CODE"]] = $arVals;
                }


                $arResult[$key]['id'] = $order["ID"];
                $arResult[$key]['id_user'] = $order["USER_ID"];
                $arResult[$key]['adspire_ip'] = $order["ID"];
                $arResult[$key]['price'] = roundEx($order["PRICE"],0);
                $arResult[$key]['usermail'] = "old";
                $arResult[$key]['email'] = md5($useremail);
                if($propsCookie['atm_marketing']){
                    $arResult[$key]['atm_marketing'] = $propsCookie['atm_marketing']['VALUE'];
                }
                if($propsCookie['atm_remarketing']){
                    $arResult[$key]['atm_remarketing'] = $propsCookie['atm_remarketing']['VALUE'];
                }
                if($propsCookie['atm_closer']){
                    $arResult[$key]['atm_closer'] = $propsCookie['atm_closer']['VALUE'];
                }
                if($propsCookie['user_ip']){
                    $arResult[$key]['adspire_ip'] = $propsCookie['user_ip']['VALUE'];
                }


            }
        }
return $arResult;
    }

    public function getOrders(){
        $orders = [];
        $dbResNew = \Bitrix\Sale\Order::getList($this->getFilter());
        while ($order = $dbResNew->fetch()){
            $orders[] = $order;
        }
        $this->setOrders($this->createArrayfromOrders($orders));
        return $this->orders;
    }

}